from setuptools import setup

setup(
    name='thumbor-s3-loader',
    version='2.0.0',
    packages=['thumbor_s3'],
    install_requires=[
        'minio',
    ]
)