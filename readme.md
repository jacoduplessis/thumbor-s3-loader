Load images from S3 using minio python library.

Steal idea from thumbor_aws to presign GET url and 
then pass that to normal http loader of thumbor.

Requires the setting of the following vars in thumbor config file:

S3_SERVER
S3_ACCESS_KEY
S3_SECRET_KEY
S3_BUCKET_NAME
S3_REGION

# Todo

- multi-tenancy by extracting bucket name from path and signing with one of a set of configured credentials
- check for more errors in the loader