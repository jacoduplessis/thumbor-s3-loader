#!/usr/bin/python
# -*- coding: utf-8 -*-

from thumbor.loaders import LoaderResult
from tornado.concurrent import return_future
from minio import Minio
from minio.error import NoSuchKey


@return_future
def load(context, path, callback):
    client = Minio(
        context.config.S3_SERVER,
        access_key=context.config.S3_ACCESS_KEY,
        secret_key=context.config.S3_SECRET_KEY,
        secure=False,
        region=context.config.S3_REGION,

    )

    result = LoaderResult()

    try:
        response = client.get_object(
            bucket_name=context.config.S3_BUCKET_NAME,
            object_name=path,
        )
        result.successful = True
        result.buffer = response.data
    except NoSuchKey:
        result.successful = False
        result.error = LoaderResult.ERROR_NOT_FOUND

    callback(result)
