#!/usr/bin/python
# -*- coding: utf-8 -*-

from datetime import timedelta

import thumbor.loaders.http_loader as http_loader
from minio import Minio
from minio.error import NoSuchKey
from thumbor.loaders import LoaderResult
from tornado.concurrent import return_future


@return_future
def load(context, path, callback):
    """
    Loads image
    :param Context context: Thumbor's context
    :param string path: Path to load
    :param callable callback: Callback method once done
    """

    client = Minio(
        context.config.S3_SERVER,
        access_key=context.config.S3_ACCESS_KEY,
        secret_key=context.config.S3_SECRET_KEY,
        secure=False,
        region=context.config.S3_REGION,

    )

    try:
        generated_url = client.presigned_get_object(
            bucket_name=context.config.S3_BUCKET_NAME,
            object_name=path,
            expires=timedelta(days=7),
        )
    except NoSuchKey:
        result = LoaderResult(successful=False, error=LoaderResult.ERROR_NOT_FOUND)
        callback(result)

    def noop(url):
        return url

    # noinspection PyUnboundLocalVariable
    http_loader.load_sync(context, generated_url, callback, normalize_url_func=noop)
